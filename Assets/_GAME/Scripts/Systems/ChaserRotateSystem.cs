using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;
namespace Asteroids
{
	public class ChaserRotateSystem : SystemBase
	{
		protected override void OnUpdate()
		{
			EntityQuery playerQuery = GetEntityQuery(ComponentType.ReadOnly<PlayerTag>());
			var playersArray = playerQuery.ToEntityArray(Allocator.TempJob);

			Entity playerEntity = Entity.Null;
			if (playersArray.Length > 0)
				playerEntity = playersArray[0];

			float deltaTime = Time.DeltaTime;
			Entities
				.WithAll<ChaserTag>()
				.ForEach((ref Rotation rotation, ref ChaserMoveData moveData, ref TargetData targetData,
				in Entity thisEntity, in LocalToWorld myLocalToWorld) =>
				{
					if (playerEntity != Entity.Null)
					{
						targetData.TargetEntity = playerEntity;

						ComponentDataFromEntity<Translation> allTranslations = GetComponentDataFromEntity<Translation>((bool)true);
						var thisTranslation = allTranslations[thisEntity];
						Translation targetTranslation = allTranslations[targetData.TargetEntity];

						if (allTranslations.HasComponent(targetData.TargetEntity) == false)
							return;//no player

					//direction
					float3 dirToTarget = targetTranslation.Value - thisTranslation.Value;
						moveData.Direction = dirToTarget;
						if (moveData.Direction.Equals(float3.zero) == false)
						{
						//use cross product to determine wether chaser rotates left or right
						var crossProduct = math.cross(myLocalToWorld.Up, moveData.Direction);
							if (crossProduct.z > 0f)
							{
								rotation.Value = math.mul(rotation.Value,
									quaternion.RotateZ(moveData.TurnSpeed * deltaTime));
							}
							else if (crossProduct.z < 0f)
							{
								rotation.Value = math.mul(rotation.Value,
									  quaternion.RotateZ(-moveData.TurnSpeed * deltaTime));
							}
						}
					}
				}).Schedule();

			playersArray.Dispose();

		}
	}
}