using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
namespace Asteroids
{
	[AlwaysSynchronizeSystem]
	public class CheckBulletOutsideBoundarySystem : JobComponentSystem
	{
		protected override JobHandle OnUpdate(JobHandle inputDeps)
		{
			var ecb = new EntityCommandBuffer(Allocator.TempJob);

			Entities
				.WithAll<BulletTag>()
				.ForEach((ref Translation myTranslation, ref Entity bulletEntity) =>
				{
					if (myTranslation.Value.x < -PlayerInputSystem.BOUNDARY_EXTENT ||
						myTranslation.Value.x > PlayerInputSystem.BOUNDARY_EXTENT ||
						myTranslation.Value.y < -PlayerInputSystem.BOUNDARY_EXTENT ||
						myTranslation.Value.y > PlayerInputSystem.BOUNDARY_EXTENT)
					{
						ecb.DestroyEntity(bulletEntity);
					}

				}).Run();

			ecb.Playback(EntityManager);
			ecb.Dispose();

			return default;
		}
	}
}