using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using Unity.Physics;
using Unity.Physics.Systems;
using UnityEngine;
namespace Asteroids
{
	public class ChaserPlayer_OnTriggerSystem : JobComponentSystem
	{
		private BuildPhysicsWorld buildPhysicsWorld;//build physics world is where we set colliders and dynamic entities
		private StepPhysicsWorld stepPhysicsWorld;//step physics world is where we run the simulation

		protected override void OnCreate()
		{
			base.OnCreate();
			buildPhysicsWorld = World.GetOrCreateSystem<BuildPhysicsWorld>();
			stepPhysicsWorld = World.GetOrCreateSystem<StepPhysicsWorld>();
		}

		protected override JobHandle OnUpdate(JobHandle inputDeps)
		{
			var newJob = new AsteroidPlayer_TriggerJob();
			newJob.allPlayers = GetComponentDataFromEntity<PlayerTag>(true);
			newJob.allChasers = GetComponentDataFromEntity<ChaserTag>(true);
			newJob.playerUpgradeData = GetSingleton<PlayerUpgradeData>();

			var ecb = new EntityCommandBuffer(Allocator.TempJob);
			newJob.ecb = ecb;

			JobHandle jobHandle = newJob.Schedule(stepPhysicsWorld.Simulation, ref buildPhysicsWorld.PhysicsWorld,
				inputDeps);

			jobHandle.Complete();

			ecb.Playback(EntityManager);
			ecb.Dispose();

			return default;
		}

		[BurstCompile]
		struct AsteroidPlayer_TriggerJob : ITriggerEventsJob
		{
			//[readonly] - these are native containers, without [readonly] no other jobs will have access to them unless we
			//release ownership to the main thread
			[ReadOnly] public ComponentDataFromEntity<PlayerTag> allPlayers;
			[ReadOnly] public ComponentDataFromEntity<ChaserTag> allChasers;
			[ReadOnly] public PlayerUpgradeData playerUpgradeData;

			public EntityCommandBuffer ecb;

			public void Execute(TriggerEvent triggerEvent)
			{
				Entity entityA = triggerEvent.EntityA;
				Entity entityB = triggerEvent.EntityB;

				if (allChasers.HasComponent(entityA) && allPlayers.HasComponent(entityB))
				{
					//entityA is chaser
					OnChaserHitsPlayer(entityA, entityB);
				}
				if (allPlayers.HasComponent(entityA) && allChasers.HasComponent(entityB))
				{
					//entityB is chaser
					OnChaserHitsPlayer(entityB, entityA);
				}
			}

			private void OnChaserHitsPlayer(Entity chaser, Entity player)
			{
				if (this.playerUpgradeData.HasShield == true)
				{
					//player has shield - destroy chaser
					ecb.DestroyEntity(chaser);
					return;
				}

				///destroy player. will trigger respawning player <see cref="PlayerSpawnerSystem"/>
				ecb.DestroyEntity(player);
			}
		}
	}
}