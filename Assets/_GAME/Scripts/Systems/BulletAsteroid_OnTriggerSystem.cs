using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using Unity.Physics;
using Unity.Physics.Systems;
using UnityEngine;
namespace Asteroids
{
	[UpdateAfter(typeof(EndFramePhysicsSystem))]//run after physics system to remove unnecessary lag
	public class BulletAsteroidOnTriggerSystem : JobComponentSystem
	{
		private BuildPhysicsWorld buildPhysicsWorld;//build physics world is where we set colliders and dynamic entities
		private StepPhysicsWorld stepPhysicsWorld;//step physics world is where we run the simulation
		private EndSimulationEntityCommandBufferSystem commandBufferSystem;

		private PrefabsData prefabsDataSingleton;

		protected override void OnCreate()
		{
			base.OnCreate();
			buildPhysicsWorld = World.GetOrCreateSystem<BuildPhysicsWorld>();
			stepPhysicsWorld = World.GetOrCreateSystem<StepPhysicsWorld>();
			commandBufferSystem = World.GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();
		}

		protected override void OnStartRunning()
		{
			base.OnStartRunning();
			prefabsDataSingleton = GetSingleton<PrefabsData>();
		}

		protected override JobHandle OnUpdate(JobHandle inputDeps)
		{
			var job = new BulletAsteroid_TriggerJob();
			job.allBullets = GetComponentDataFromEntity<BulletTag>(true);
			job.allBulletData = GetComponentDataFromEntity<BulletData>(true);
			job.allAsteroids = GetComponentDataFromEntity<AsteroidTag>(true);
			job.allAsteroidData = GetComponentDataFromEntity<AsteroidData>();
			job.allTranslations = GetComponentDataFromEntity<Translation>(true);
			job.allPhysicsVelocities = GetComponentDataFromEntity<PhysicsVelocity>(true);

			job.ecb = commandBufferSystem.CreateCommandBuffer();
			job.prefabsSingleton = this.prefabsDataSingleton;

			JobHandle jobHandle = job.Schedule(stepPhysicsWorld.Simulation,
				ref buildPhysicsWorld.PhysicsWorld,
				inputDeps);

			commandBufferSystem.AddJobHandleForProducer(jobHandle);//run commandBufferSystem when it's safe to run it,
																   //you don't have to wait for job when it's
																   //complete with "jobHandle.Complete();" just for when it's safe
			return jobHandle;
		}

		[BurstCompile]
		struct BulletAsteroid_TriggerJob : ITriggerEventsJob
		{
			//[readonly] - these are native containers, without [readonly] no other jobs will have access to them unless we
			//release ownership to the main thread
			[ReadOnly] public ComponentDataFromEntity<BulletTag> allBullets;
			[ReadOnly] public ComponentDataFromEntity<BulletData> allBulletData;
			[ReadOnly] public ComponentDataFromEntity<AsteroidTag> allAsteroids;
			[ReadOnly] public ComponentDataFromEntity<AsteroidData> allAsteroidData;
			[ReadOnly] public ComponentDataFromEntity<Translation> allTranslations;
			[ReadOnly] public ComponentDataFromEntity<PhysicsVelocity> allPhysicsVelocities;
			[ReadOnly] public PrefabsData prefabsSingleton;

			public EntityCommandBuffer ecb;

			public void Execute(TriggerEvent triggerEvent)
			{
				Entity entityA = triggerEvent.EntityA;
				Entity entityB = triggerEvent.EntityB;

				if (allBullets.HasComponent(entityA) && allBullets.HasComponent(entityB))
					return;//2 bullets hit each other

				if (allBullets.HasComponent(entityA) && allAsteroids.HasComponent(entityB))
				{
					//entityA is bullet
					OnBulletHitsAsteroid(entityA, entityB);
				}
				else if (allAsteroids.HasComponent(entityA) && allBullets.HasComponent(entityB))
				{
					//entityB is pickup
					OnBulletHitsAsteroid(entityB, entityA);
				}
			}

			private void OnBulletHitsAsteroid(Entity bullet, Entity asteroid)
			{
				BulletData bulletData = allBulletData[bullet];
				AsteroidData shotAsteroidData = allAsteroidData[asteroid];

				if (bulletData.MyType != BulletType.SuperBullet &&//superbullet destroys asteroid
					shotAsteroidData.MyType != AsteroidType.Small)//small asteroid doesn't go into two smaller
				{
					//create two smaller asteroids
					CreateAsteroidAfterHit(prefabsSingleton,
						allTranslations[asteroid],
						allPhysicsVelocities[asteroid],
						30,//new asteroid goes 30 degrees to the left of old asteroid's speed
						shotAsteroidData);
					CreateAsteroidAfterHit(prefabsSingleton,
						allTranslations[asteroid],
						allPhysicsVelocities[asteroid],
						-30,//new asteroid goes 30 degrees to the right of old asteroid's speed
						shotAsteroidData);
				}

				//destroy bullet
				ecb.DestroyEntity(bullet);

				//destroy asteroid
				ecb.DestroyEntity(asteroid);
			}

			/// <param name="angleOffset">Angle Offset to the speed of hit asteroid</param>
			private void CreateAsteroidAfterHit(PrefabsData asteroidPrefabs,
				Translation shotAsteroidTranslation,
				PhysicsVelocity shotAsteroidVelocity, float angleOffset, AsteroidData shotAsteroidData)
			{
				//spawn new asteroid
				Entity newAsteroid = Entity.Null;
				switch (shotAsteroidData.MyType)
				{
					case AsteroidType.Big:
						//create medium asteroid
						newAsteroid = ecb.Instantiate(asteroidPrefabs.MediumAsteroidPrefab);
						AsteroidData newAsteroidData = new AsteroidData()
						{
							MyType = AsteroidType.Medium,
						};
						ecb.AddComponent(newAsteroid, newAsteroidData);
						break;
					case AsteroidType.Medium:
						//create small asteroid
						newAsteroid = ecb.Instantiate(asteroidPrefabs.SmallAsteroidPrefab);
						newAsteroidData = new AsteroidData()
						{
							MyType = AsteroidType.Small,
						};
						ecb.AddComponent(newAsteroid, newAsteroidData);
						break;
				}

				//initial speed
				var asteroidSpeed = shotAsteroidVelocity.Linear;
				var newSpeed = math.rotate(quaternion.RotateZ(math.radians(angleOffset)), asteroidSpeed);
				PhysicsVelocity velocity = new PhysicsVelocity()
				{
					Linear = newSpeed,
					Angular = Unity.Mathematics.float3.zero
				};
				ecb.AddComponent(newAsteroid, velocity);

				//initial position
				//offset initial position perpendicularly to asteroidSpeed to prevent new asteroids from
				//bumping into each other at initialization
				float3 perpendicularOffset = float3.zero;
				if (angleOffset > 0)
					perpendicularOffset =
						math.normalize(math.rotate(quaternion.RotateZ(math.radians(90)), asteroidSpeed)) * .5f;
				else
					perpendicularOffset =
						math.normalize(math.rotate(quaternion.RotateZ(math.radians(-90)), asteroidSpeed)) * .5f;
				Translation position = new Translation()
				{
					Value = shotAsteroidTranslation.Value + perpendicularOffset,
				};
				ecb.AddComponent(newAsteroid, position);

			}
		}
	}
}