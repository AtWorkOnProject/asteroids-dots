using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using Unity.Physics;
using Unity.Physics.Systems;
using UnityEngine;
namespace Asteroids
{
	[UpdateAfter(typeof(EndFramePhysicsSystem))]//run after physics system to remove unnecessary lag
	public class PlayerPickupOnTriggerSystem : JobComponentSystem
	{
		private BuildPhysicsWorld buildPhysicsWorld;//build physics world is where we set colliders and dynamic entities
		private StepPhysicsWorld stepPhysicsWorld;//step physics world is where we run the simulation
		private EndSimulationEntityCommandBufferSystem commandBufferSystem;

		protected override void OnCreate()
		{
			base.OnCreate();
			buildPhysicsWorld = World.GetOrCreateSystem<BuildPhysicsWorld>();
			stepPhysicsWorld = World.GetOrCreateSystem<StepPhysicsWorld>();
			commandBufferSystem = World.GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();
		}

		[BurstCompile]//[when it was comented] Burst doesn't run on main thread so Debug.Log doesn't work
		struct PlayerPickup_TriggerJob : ITriggerEventsJob
		{
			//[readonly] - these are native containers, without [readonly] no other jobs will have access to them unless we
			//release ownership to the main thread
			[ReadOnly] public ComponentDataFromEntity<PickupTag> allPickups;
			[ReadOnly] public ComponentDataFromEntity<PickupData> allPickupData;
			[ReadOnly] public ComponentDataFromEntity<PlayerTag> allPlayers;
			public NativeArray<PlayerUpgradeData> upgradeDataResult;
			public EntityCommandBuffer entityCommandBuffer;

			public void Execute(TriggerEvent triggerEvent)//Execute() doesn't run if there are no triggers triggered!
			{
				Entity entityA = triggerEvent.EntityA;
				Entity entityB = triggerEvent.EntityB;

				if (allPickups.HasComponent(entityA) && allPickups.HasComponent(entityB))
					return;//2 pickups meet

				if (allPickups.HasComponent(entityA) && allPlayers.HasComponent(entityB))
				{
					//entityA is pickup
					OnPlayerGainedPickUp(entityB, entityA);
				}
				else if (allPlayers.HasComponent(entityA) && allPickups.HasComponent(entityB))
				{
					//entityB is pickup
					OnPlayerGainedPickUp(entityA, entityB);
				}
			}

			private void OnPlayerGainedPickUp(Entity playerEntity, Entity PickupEntity)
			{
				var pickupData = allPickupData[PickupEntity];

				PlayerUpgradeData resultingUpgradeData = upgradeDataResult[0];
				switch (pickupData.MyType)
				{
					case PickupType.Shield:
						resultingUpgradeData.HasShield = true;
						resultingUpgradeData.HasShieldAgo = 0f;
						break;
					case PickupType.SuperBullet:
						resultingUpgradeData.HasSuperBullet = true;
						resultingUpgradeData.HasSuperBulletAgo = 0f;
						break;
				}
				upgradeDataResult[0] = resultingUpgradeData;

				entityCommandBuffer.DestroyEntity(PickupEntity);
			}
		}


		protected override JobHandle OnUpdate(JobHandle inputDeps)
		{
			var job = new PlayerPickup_TriggerJob();

			job.allPickups = GetComponentDataFromEntity<PickupTag>(true);
			job.allPlayers = GetComponentDataFromEntity<PlayerTag>(true);
			job.allPickupData = GetComponentDataFromEntity<PickupData>(true);
			job.entityCommandBuffer = new EntityCommandBuffer(Allocator.TempJob);
			job.upgradeDataResult = new NativeArray<PlayerUpgradeData>(1, Allocator.TempJob);
			job.upgradeDataResult[0] = GetSingleton<PlayerUpgradeData>();

			JobHandle handle = job.Schedule(stepPhysicsWorld.Simulation, ref buildPhysicsWorld.PhysicsWorld,
				inputDeps);

			handle.Complete();

			//update player upgrade data
			var entities = GetEntityQuery(typeof(PlayerUpgradeData)).ToEntityArray(Allocator.Temp);
			EntityManager.AddComponentData<PlayerUpgradeData>(
				entities[0],
				job.upgradeDataResult[0]
				);
			entities.Dispose();
			job.upgradeDataResult.Dispose();

			job.entityCommandBuffer.Playback(EntityManager);
			job.entityCommandBuffer.Dispose();

			return handle;
		}
	}
}