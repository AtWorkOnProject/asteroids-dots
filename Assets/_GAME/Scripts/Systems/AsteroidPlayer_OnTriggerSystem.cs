using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using Unity.Physics;
using Unity.Physics.Systems;
using UnityEngine;

namespace Asteroids
{
	public class AsteroidPlayer_OnTriggerSystem : JobComponentSystem
	{
		private BuildPhysicsWorld buildPhysicsWorld;//build physics world is where we set colliders and dynamic entities
		private StepPhysicsWorld stepPhysicsWorld;//step physics world is where we run the simulation

		protected override void OnCreate()
		{
			base.OnCreate();
			buildPhysicsWorld = World.GetOrCreateSystem<BuildPhysicsWorld>();
			stepPhysicsWorld = World.GetOrCreateSystem<StepPhysicsWorld>();
		}

		protected override JobHandle OnUpdate(JobHandle inputDeps)
		{
			var newJob = new AsteroidPlayer_TriggerJob();
			newJob.allPlayers = GetComponentDataFromEntity<PlayerTag>(true);
			newJob.allAsteroids = GetComponentDataFromEntity<AsteroidTag>(true);
			newJob.playerUpgradeData = GetSingleton<PlayerUpgradeData>();

			var ecb = new EntityCommandBuffer(Allocator.TempJob);
			newJob.ecb = ecb;

			JobHandle jobHandle = newJob.Schedule(stepPhysicsWorld.Simulation, ref buildPhysicsWorld.PhysicsWorld,
				inputDeps);

			jobHandle.Complete();

			ecb.Playback(EntityManager);
			ecb.Dispose();

			return default;
		}

		[BurstCompile]
		struct AsteroidPlayer_TriggerJob : ITriggerEventsJob
		{
			//[readonly] - these are native containers, without [readonly] no other jobs will have access to them unless we
			//release ownership to the main thread
			[ReadOnly] public ComponentDataFromEntity<PlayerTag> allPlayers;
			[ReadOnly] public ComponentDataFromEntity<AsteroidTag> allAsteroids;
			[ReadOnly] public PlayerUpgradeData playerUpgradeData;

			public EntityCommandBuffer ecb;

			public void Execute(TriggerEvent triggerEvent)
			{
				Entity entityA = triggerEvent.EntityA;
				Entity entityB = triggerEvent.EntityB;

				if (allAsteroids.HasComponent(entityA) && allPlayers.HasComponent(entityB))
				{
					//entityA is asteroid
					OnAsteroidHitsPlayer(entityA, entityB);
				}
				if (allPlayers.HasComponent(entityA) && allAsteroids.HasComponent(entityB))
				{
					//entityB is asteroid
					OnAsteroidHitsPlayer(entityB, entityA);
				}
			}

			private void OnAsteroidHitsPlayer(Entity asteroid, Entity player)
			{
				if (this.playerUpgradeData.HasShield == true)
				{
					//player has shield - destroy only asteroid
					ecb.DestroyEntity(asteroid);
					return;
				}

				///destroy player. will trigger respawning player <see cref="PlayerSpawnerSystem"/>
				ecb.DestroyEntity(player);
			}
		}
	}
}