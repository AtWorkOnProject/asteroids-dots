using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Physics;
using Unity.Transforms;
using UnityEngine;
namespace Asteroids
{
	/// <summary>
	/// Hyperspace Jump is handlesd in <see cref="PlayerInput_HyperspaceJumpSystem"/>
	/// </summary>
	[AlwaysSynchronizeSystem]//it's main thread, finish the remaining work that has to happen before this one
							 //and then the next system can do its thing
	public class PlayerInputSystem : JobComponentSystem
	{
		private PrefabsData prefabsSingleton;

		protected override void OnStartRunning()
		{
			base.OnStartRunning();

			prefabsSingleton = GetSingleton<PrefabsData>();
		}

		protected override JobHandle OnUpdate(JobHandle inputDeps)
		{
			float deltaTime = Time.DeltaTime;
			float acceleration = .1f;
			float turnSpeed = 300.0f;

			EntityCommandBuffer ecb = new EntityCommandBuffer(Allocator.TempJob);
			var playerUpgradeData = GetSingleton<PlayerUpgradeData>();
			Entity superBulletPrefab = prefabsSingleton.SuperBulletPrefab;
			Entity normalBulletPrefab = prefabsSingleton.BulletPrefab;

			Entities
				.WithAll<PlayerTag>()
				//read from inputData ("ref"), white to moveData ("in")
				.ForEach((ref PlayerMoveData myMoveData, ref Translation myTranslation, ref Rotation myRotation,
				in LocalToWorld myLocalToWorld) =>
			{
			//shoot bullets
			if (Input.GetKeyDown(KeyCode.Space))
				{

					Entity newBullet = Entity.Null;
					if (playerUpgradeData.HasSuperBullet)
						newBullet = ecb.Instantiate(superBulletPrefab);
					else
						newBullet = ecb.Instantiate(normalBulletPrefab);

				//initial position
				Translation position = new Translation()
					{
						Value = myTranslation.Value,
					};
					ecb.AddComponent(newBullet, position);
				//initial speed
				Vector3 speed = myLocalToWorld.Up * 5f;
					PhysicsVelocity velocity = new PhysicsVelocity()
					{
						Linear = speed,
						Angular = Unity.Mathematics.float3.zero
					};
					ecb.AddComponent(newBullet, velocity);
				}

			//rotation
			if (Input.GetKey(KeyCode.LeftArrow))
				{
					myRotation.Value = math.mul(myRotation.Value,
						quaternion.RotateZ(math.radians(turnSpeed * deltaTime))
						);
				}
				if (Input.GetKey(KeyCode.RightArrow))
				{
					myRotation.Value = math.mul(myRotation.Value,
						quaternion.RotateZ(-math.radians(turnSpeed * deltaTime))
						);
				}

			//acceleration
			if (Input.GetKey(KeyCode.UpArrow))
				{
					myMoveData.InertiaSpeed += myLocalToWorld.Up * acceleration * deltaTime;
				}
				if (Input.GetKey(KeyCode.DownArrow))
				{
					myMoveData.InertiaSpeed -= myLocalToWorld.Up * acceleration * deltaTime;
				}

			//update position
			myTranslation.Value += myMoveData.InertiaSpeed;

			//wrap player's position
			if (myTranslation.Value.x < -BOUNDARY_EXTENT)
					myTranslation.Value.x += HOW_MUCH_TELEPORT_WHEN_REACHED_BOUNDARY;
				if (myTranslation.Value.x > BOUNDARY_EXTENT)
					myTranslation.Value.x -= HOW_MUCH_TELEPORT_WHEN_REACHED_BOUNDARY;
				if (myTranslation.Value.y < -BOUNDARY_EXTENT)
					myTranslation.Value.y += HOW_MUCH_TELEPORT_WHEN_REACHED_BOUNDARY;
				if (myTranslation.Value.y > BOUNDARY_EXTENT)
					myTranslation.Value.y -= HOW_MUCH_TELEPORT_WHEN_REACHED_BOUNDARY;
			})
				//.Schedule(inputDeps);//run on worker threads
				.Run();//run on main thread. We access things outside our worker threads (Input.GetKey())
					   //so we need to run it on main thread. also we have simple system, with few (1) entities
					   //if we run on worker threads, we wouldn't use [AlwaysSynchronizeSystem] class attribute

			ecb.Playback(EntityManager);
			ecb.Dispose();

			return default;// with [AlwasySynchronizeSystem] class attribute we could do "return inputDepts;" but
						   //now it's clear that we're not using input dependencies from previous jobs for anything

			//return inputDeps;//we're running on main thread => next job will have to do some safety checks
			//e.g. if we're doing async stuff that must be finished before next job

			//we're on main thread: we could wrap up everything inputDeps Job Handle is doing and return new one for this job,
			//but new way is using [AlwaysSynchronizeSystem] attribute
			//inputDeps.Complete();
			//return new JobHandle();
			///Complete(): wait in main thread for inputDeps Job Handle to finish executing. after
			//that we can safely access NativeContainer (shared memory for jobs so we can access jobs' results)
			//that job was using
			//2nd explanation: Complete() flushes job from memory, returns ownership of native containers back to main thread,
			//and basically makes things save again
			//3rs explanation: after complete() you can access results of a job (when job is struct with Execute(), its
			//result fields are safe to access) see https://youtu.be/E8VNVTsRE4I
		}

		//based on position of boundary
		public static readonly float BOUNDARY_EXTENT = 6f;
		public static readonly float HOW_MUCH_TELEPORT_WHEN_REACHED_BOUNDARY = 11.9f;//not 12.0 to remove flickering on boundary

	}
}