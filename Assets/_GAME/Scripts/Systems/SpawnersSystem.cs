using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;
namespace Asteroids
{
	/// <summary>
	/// Access spawners (which have easy access to spawn points) and tell them to spawn asteroids / pickups
	/// 
	/// </summary>
	[AlwaysSynchronizeSystem]
	public class SpawnersSystem : JobComponentSystem
	{
		private PrefabsData prefabsSingleton;
		protected override void OnStartRunning()
		{
			base.OnStartRunning();
			prefabsSingleton = GetSingleton<PrefabsData>();
		}
		protected override JobHandle OnUpdate(JobHandle inputDeps)
		{
			EntityQuery playerQuery = GetEntityQuery(ComponentType.ReadOnly<PlayerTag>());
			var playersArray = playerQuery.ToEntityArray(Allocator.TempJob);
			if (playersArray.Length > 0)
			{
				if (AsteroidSpawner.Instance == null || PickupSpawner.Instance == null)
				{
					playersArray.Dispose();
					return default;//don't spawn asteroid when there is no player
				}

				PickupSpawner.Instance.SpawnPickup_EverySomeTime(prefabsSingleton);
				ChaserSpawner.Instance.SpawnChaser_EverySomeTime(prefabsSingleton);

				///for spawning 2 smaller asteroids on bullet hit < see cref = "BulletAsteroidOnTriggerSystem" />
				AsteroidSpawner.Instance.SpawnAsteroid_EverySomeTime(prefabsSingleton.BigAsteroidPrefab);
			}
			else
			{
				AsteroidSpawner.Instance.ResetSpawnedAsteroidsNumber();
			}

			playersArray.Dispose();
			return default;
		}
	}
}