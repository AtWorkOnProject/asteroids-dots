using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

namespace Asteroids
{
	/// <summary>
	/// counts collected upgrades in <see cref="PlayerUpgradeData"/> and switches them off 
	/// after upgrades run out
	/// </summary>
	[AlwaysSynchronizeSystem]
	public class PlayerUpgradeSystem : JobComponentSystem
	{
		protected override JobHandle OnUpdate(JobHandle inputDeps)
		{
			float deltaTime = Time.DeltaTime;

			Entities
				.ForEach((ref PlayerUpgradeData upgradeData) =>
				{
					if (upgradeData.HasShield)
					{
						upgradeData.HasShieldAgo += deltaTime;
						if (upgradeData.HasShieldAgo >= 6.0f)
						{
							upgradeData.HasShield = false;
							upgradeData.HasSuperBulletAgo = 0f;
						}
					}
					if (upgradeData.HasSuperBullet)
					{
						upgradeData.HasSuperBulletAgo += deltaTime;
						if (upgradeData.HasSuperBulletAgo >= 6.0f)
						{
							upgradeData.HasSuperBullet = false;
							upgradeData.HasSuperBulletAgo = 0f;
						}
					}
				}).Run();
			return default;
		}
	}
}