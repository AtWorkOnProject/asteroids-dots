using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;
namespace Asteroids
{
	/// <summary>
	/// checks if player is in the scene. if No player, spawn him
	/// </summary>
	[AlwaysSynchronizeSystem]
	public class PlayerSpawnerSystem : JobComponentSystem
	{
		protected override JobHandle OnUpdate(JobHandle inputDeps)
		{
			Entity playerEntity = Entity.Null;
			EntityQuery playerQuery = GetEntityQuery(ComponentType.ReadOnly<PlayerTag>());
			var playersArray = playerQuery.ToEntityArray(Allocator.TempJob);
			if (playersArray.Length > 0)
				playerEntity = playersArray[0];

			var asteroidsQuery = GetEntityQuery(ComponentType.ReadOnly<AsteroidTag>());
			var asteroidsArray = asteroidsQuery.ToEntityArray(Allocator.TempJob);
			var chasersQuery = GetEntityQuery(ComponentType.ReadOnly<ChaserTag>());
			var chasersArray = chasersQuery.ToEntityArray(Allocator.TempJob);
			var pickupsQuery = GetEntityQuery(ComponentType.ReadOnly<PickupTag>());
			var pickupsArray = pickupsQuery.ToEntityArray(Allocator.TempJob);

			var prefabsSingleton = GetSingleton<PrefabsData>();
			var ecb = new EntityCommandBuffer(Allocator.TempJob);

			float deltaTime = Time.DeltaTime;

			Entities
				//.WithStructuralChanges()//Create or destroy component or entity is structural change: something that modifies how
				// data is stored in memory (where everything is organized by archetype [identifier for
				// unique combination of data components])
				// We'll use EntityCommandBuffer because WithStructuralChanges() is slow
				// because it makes specialized copy of data that allows for EntityManager.DestroyEntity(entity))
				.ForEach((ref PlayerSpawnerData spawnerData) =>
				{
				//if no player, spawn player after some time
				if (playerEntity == Entity.Null)
					{
						spawnerData.TimeOfNoPlayer += deltaTime;
						if (spawnerData.TimeOfNoPlayer >= 1.0f)
						{
							spawnerData.TimeOfNoPlayer = 0f;

						//destroy all asteroids
						for (int i = asteroidsArray.Length - 1; i >= 0; i--)
								ecb.DestroyEntity(asteroidsArray[i]);

						//destroy all chasers
						for (int i = chasersArray.Length - 1; i >= 0; i--)
								ecb.DestroyEntity(chasersArray[i]);

						//destroy all pickups
						for (int i = pickupsArray.Length - 1; i >= 0; i--)
								ecb.DestroyEntity(pickupsArray[i]);

						//spawn player
						Entity newPlayer = ecb.Instantiate(prefabsSingleton.PlayerPrefab);
							Translation position = new Translation()//initial position
						{
								Value = float3.zero,
							};
							ecb.AddComponent(newPlayer, position);
						}
					}
					else
					{
						spawnerData.TimeOfNoPlayer = 0f;
					}
				}).Run();

			ecb.Playback(EntityManager);
			ecb.Dispose();

			playersArray.Dispose();
			asteroidsArray.Dispose();
			chasersArray.Dispose();
			pickupsArray.Dispose();

			return default;
		}
	}
}