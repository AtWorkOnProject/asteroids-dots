using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
namespace Asteroids
{
	public class CheckAsteroidOutsideBoundarySystem : JobComponentSystem
	{
		protected override JobHandle OnUpdate(JobHandle inputDeps)
		{
			var jobHandle = Entities
				.WithAll<AsteroidTag>()
				.ForEach((ref Translation myTranslation, ref Entity asteroidEntity) =>
				{
					if (myTranslation.Value.x < -PlayerInputSystem.BOUNDARY_EXTENT)
						myTranslation.Value.x += PlayerInputSystem.HOW_MUCH_TELEPORT_WHEN_REACHED_BOUNDARY;
					if (myTranslation.Value.x > PlayerInputSystem.BOUNDARY_EXTENT)
						myTranslation.Value.x -= PlayerInputSystem.HOW_MUCH_TELEPORT_WHEN_REACHED_BOUNDARY;
					if (myTranslation.Value.y < -PlayerInputSystem.BOUNDARY_EXTENT)
						myTranslation.Value.y += PlayerInputSystem.HOW_MUCH_TELEPORT_WHEN_REACHED_BOUNDARY;
					if (myTranslation.Value.y > PlayerInputSystem.BOUNDARY_EXTENT)
						myTranslation.Value.y -= PlayerInputSystem.HOW_MUCH_TELEPORT_WHEN_REACHED_BOUNDARY;
				}).Schedule(inputDeps);

			return jobHandle;
		}
	}
}