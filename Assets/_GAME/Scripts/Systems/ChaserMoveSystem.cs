using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
namespace Asteroids
{
	public class ChaserSystem : SystemBase
	{
		protected override void OnUpdate()
		{
			EntityQuery playerQuery = GetEntityQuery(ComponentType.ReadOnly<PlayerTag>());
			var playersArray = playerQuery.ToEntityArray(Allocator.TempJob);

			Entity playerEntity = Entity.Null;
			if (playersArray.Length > 0)
				playerEntity = playersArray[0];

			float deltaTime = Time.DeltaTime;

			Entities
				.WithAll<ChaserTag>()
				.ForEach((ref Rotation rotation, ref ChaserMoveData moveData, ref TargetData targetData,
				ref Translation thisTranslation, in Entity thisEntity, in LocalToWorld myLocalToWorld) =>
				{
					if (playerEntity != Entity.Null)
					{
						if (moveData.Direction.Equals(float3.zero) == false)
						{
						//position
						//toremove, old code going always to player
						//float3 normalizedDir = math.normalize(moveData.direction);
						//thisTranslation.Value += normalizedDir * moveData.speed * deltaTime;
						thisTranslation.Value += myLocalToWorld.Up * moveData.Speed * deltaTime;
						}
					}
				}).Schedule();

			playersArray.Dispose();

		}
	}
}