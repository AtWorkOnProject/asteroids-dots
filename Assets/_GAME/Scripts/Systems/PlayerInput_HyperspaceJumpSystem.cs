using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Physics;
using Unity.Transforms;
using UnityEngine;
namespace Asteroids
{
	/// <summary>
	/// All other input is handlesd in <see cref="PlayerInput_HyperspaceJumpSystem"/>
	/// </summary>
	[AlwaysSynchronizeSystem]
	public class PlayerInput_HyperspaceJumpSystem : JobComponentSystem
	{
		protected override void OnStartRunning()
		{
			base.OnStartRunning();
		}

		protected override JobHandle OnUpdate(JobHandle inputDeps)
		{
			if (Input.GetKeyDown(KeyCode.H))
			{
				ComponentDataFromEntity<Translation> allTranslations = GetComponentDataFromEntity<Translation>(true);

				var playerQuery = GetEntityQuery(ComponentType.ReadOnly<PlayerTag>());
				var playerArray = playerQuery.ToEntityArray(Allocator.TempJob);
				if (playerArray.Length < 1)
				{
					playerArray.Dispose();
					return inputDeps;
				}
				var playerEntity = playerArray[0];
				var playerTranslation = allTranslations[playerEntity];

				//find good position (not on asteroid)
				bool isPositionGood = false;
				float3 positionCandidate = float3.zero;
				int antiinfinite = 0;
				while (isPositionGood == false && antiinfinite++ < 30)
				{
					positionCandidate = new float3(
						UnityEngine.Random.Range(-5f, 5f),
						UnityEngine.Random.Range(-5f, 5f),
						0
						);

					isPositionGood = true;
					Entities
						.WithAll<AsteroidTag>()
						.ForEach((ref Translation myTranslation) =>
					{
						if (math.distancesq(positionCandidate, myTranslation.Value) < 2.0f * 2.0f)
						{
							isPositionGood = false;
						}
					})
					.Run();

					if (antiinfinite >= 29)
						Debug.Log("antiinfinite. normal if there are many asteroids");
				}
				playerArray.Dispose();

				if (isPositionGood)
				{
					//perform jump
					Entities
						.WithAll<PlayerTag>()
						.ForEach((ref Translation myTranslation, ref PlayerMoveData moveData) =>
						{
							myTranslation.Value = positionCandidate;
							moveData.InertiaSpeed = float3.zero;
						})
					.Run();
				}
			}

			return default;
		}
	}
}