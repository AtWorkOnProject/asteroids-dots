using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using Unity.Physics;
using Unity.Physics.Systems;
using UnityEngine;
namespace Asteroids
{
	public class BulletChaserOnTriggerSystem : JobComponentSystem
	{
		private BuildPhysicsWorld buildPhysicsWorld;//build physics world is where we set colliders and dynamic entities
		private StepPhysicsWorld stepPhysicsWorld;//step physics world is where we run the simulation
		private EndSimulationEntityCommandBufferSystem commandBufferSystem;

		protected override void OnCreate()
		{
			base.OnCreate();
			buildPhysicsWorld = World.GetOrCreateSystem<BuildPhysicsWorld>();
			stepPhysicsWorld = World.GetOrCreateSystem<StepPhysicsWorld>();
			commandBufferSystem = World.GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();
		}

		protected override JobHandle OnUpdate(JobHandle inputDeps)
		{
			var job = new BulletChaser_TriggerJob();
			job.allBullets = GetComponentDataFromEntity<BulletTag>(true);
			job.allChasers = GetComponentDataFromEntity<ChaserTag>(true);

			job.ecb = commandBufferSystem.CreateCommandBuffer();

			JobHandle jobHandle = job.Schedule(stepPhysicsWorld.Simulation,
				ref buildPhysicsWorld.PhysicsWorld,
				inputDeps);

			commandBufferSystem.AddJobHandleForProducer(jobHandle);
			return jobHandle;
		}

		[BurstCompile]
		struct BulletChaser_TriggerJob : ITriggerEventsJob
		{
			[ReadOnly] public ComponentDataFromEntity<BulletTag> allBullets;
			[ReadOnly] public ComponentDataFromEntity<ChaserTag> allChasers;

			public EntityCommandBuffer ecb;

			public void Execute(TriggerEvent triggerEvent)
			{
				Entity entityA = triggerEvent.EntityA;
				Entity entityB = triggerEvent.EntityB;

				if (allBullets.HasComponent(entityA) && allChasers.HasComponent(entityB))
				{
					//entityA is bullet
					OnBulletHitsChaser(entityA, entityB);
				}
				else if (allChasers.HasComponent(entityA) && allBullets.HasComponent(entityB))
				{
					//entityB is bullet
					OnBulletHitsChaser(entityB, entityA);
				}
			}

			private void OnBulletHitsChaser(Entity bullet, Entity chaser)
			{
				//destroy chaser and bullet
				ecb.DestroyEntity(chaser);
				ecb.DestroyEntity(bullet);
			}
		}
	}
}