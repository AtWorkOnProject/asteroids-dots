using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Rendering;
using Unity.Transforms;
using UnityEngine;

namespace Asteroids
{
	[AlwaysSynchronizeSystem]
	public class PlayerShieldSystem : JobComponentSystem
	{
		protected override JobHandle OnUpdate(JobHandle inputDeps)
		{
			var em = EntityManager;
			var ecb = new EntityCommandBuffer(Allocator.TempJob);

			Entities
				.WithAll<PlayerShieldTag>()
				.WithStructuralChanges()//for SetEnabled()
				.ForEach((in PlayerShieldTargetData shieldTarget) =>
			{
				if (GetSingleton<PlayerUpgradeData>().HasShield == true)
				{
					em.SetEnabled(shieldTarget.TargetEntity, true);
				}
				else
				{
					em.SetEnabled(shieldTarget.TargetEntity, false);
				}
			}).Run();

			ecb.Playback(EntityManager);
			ecb.Dispose();

			return default;
		}
	}
}