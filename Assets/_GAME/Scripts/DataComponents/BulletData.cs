using Unity.Entities;
using Unity.Mathematics;
namespace Asteroids
{
	[GenerateAuthoringComponent]
	public struct BulletData : IComponentData
	{
		public BulletType MyType;
	}
	public enum BulletType
	{
		NormalBullet,
		SuperBullet,
	}
}