using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;
namespace Asteroids
{
	[GenerateAuthoringComponent]
	public struct ChaserMoveData : IComponentData
	{
		public float3 Direction;
		public float Speed;
		public float TurnSpeed;
	}
}