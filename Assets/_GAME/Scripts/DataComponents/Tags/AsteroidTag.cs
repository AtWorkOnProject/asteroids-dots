using Unity.Entities;
namespace Asteroids
{
	/// <summary>
	/// no data, just for ForEach queries to find everything that is an asteroid 
	/// </summary>
	[GenerateAuthoringComponent]
	public struct AsteroidTag : IComponentData
	{

	}
}