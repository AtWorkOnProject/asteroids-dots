using Unity.Entities;
namespace Asteroids
{
	/// <summary>
	/// no data, just for ForEach queries 
	/// </summary>
	[GenerateAuthoringComponent]
	public struct PickupTag : IComponentData
	{

	}
}