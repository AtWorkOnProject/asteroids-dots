using Unity.Entities;
using Unity.Mathematics;
namespace Asteroids
{
	[GenerateAuthoringComponent]
	public struct PickupData : IComponentData
	{
		public PickupType MyType;
	}
	public enum PickupType
	{
		SuperBullet,
		Shield
	}
}