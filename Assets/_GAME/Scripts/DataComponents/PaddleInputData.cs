using Unity.Entities;
using UnityEngine;
namespace Asteroids
{
	[GenerateAuthoringComponent]
	public struct PaddleInputData : IComponentData
	{
		public KeyCode UpKey;
		public KeyCode DownKey;
	}
}