using System;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;
namespace Asteroids
{
	[GenerateAuthoringComponent]
	public struct PrefabsData : IComponentData
	{
		public Entity BigAsteroidPrefab;
		public Entity MediumAsteroidPrefab;
		public Entity SmallAsteroidPrefab;
		[Space]
		public Entity PlayerPrefab;
		[Space]
		public Entity BulletPrefab;
		public Entity SuperBulletPrefab;
		[Space]
		public Entity PickupPrefab_SuperBullet;
		public Entity PickupPrefab_Shield;
		[Space]
		public Entity ChaserPrefab;
	}
}