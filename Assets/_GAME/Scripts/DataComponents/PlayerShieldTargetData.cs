using System;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
namespace Asteroids
{
	[GenerateAuthoringComponent]
	public struct PlayerShieldTargetData : IComponentData
	{
		public Entity TargetEntity;
	}
}