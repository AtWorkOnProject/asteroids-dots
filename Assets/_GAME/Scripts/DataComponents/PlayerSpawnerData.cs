using System;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
namespace Asteroids
{
	/// <summary>
	/// singleton for managing spawning player, counts time while player is not in the scene
	/// helps <see cref="PlayerSpawnerSystem"/>
	/// </summary>
	[GenerateAuthoringComponent]
	public struct PlayerSpawnerData : IComponentData
	{
		public float TimeOfNoPlayer;
	}
}