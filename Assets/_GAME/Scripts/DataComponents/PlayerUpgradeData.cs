using System;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;
namespace Asteroids
{
	[GenerateAuthoringComponent]
	public struct PlayerUpgradeData : IComponentData
	{
		public bool HasShield;
		public float HasShieldAgo;
		[Space]
		public bool HasSuperBullet;
		public float HasSuperBulletAgo;
	}
}