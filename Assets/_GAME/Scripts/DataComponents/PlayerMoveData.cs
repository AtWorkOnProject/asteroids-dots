using Unity.Entities;
using Unity.Mathematics;
namespace Asteroids
{
	[GenerateAuthoringComponent]
	public struct PlayerMoveData : IComponentData
	{
		public float3 InertiaSpeed;
	}
}