using Unity.Entities;
using Unity.Mathematics;
namespace Asteroids
{
	[GenerateAuthoringComponent]
	public struct AsteroidData : IComponentData
	{
		public AsteroidType MyType;
	}
	public enum AsteroidType
	{
		Big,
		Medium,
		Small,
	}
}