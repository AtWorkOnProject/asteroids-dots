using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using Unity.Physics;
using Unity.Transforms;
using UnityEngine;
namespace Asteroids
{
	public class AsteroidSpawner : MonoBehaviour
	{
		public static AsteroidSpawner Instance;

		[SerializeField] private AsteroidSpawnPoint[] spawnPoints;
		[SerializeField] private Vector2 initialSpeedMinMax = new Vector2(1.5f, 2.5f);
		[SerializeField] private int howManyAsteroidsToSpawn = 4;
		[SerializeField] private float timeToSpawnNext = 1.5f;

		private int spawnedAsteroidsNumber;
		private EntityManager myEntityManager;
		private float timeLastAsteroidSpawned;

		public bool AreThereAsteroidsToSpawn => spawnedAsteroidsNumber < howManyAsteroidsToSpawn;

		private void Awake()
		{
			Instance = this;

			myEntityManager = World.DefaultGameObjectInjectionWorld//give me world where GOs converted into entities go
				.EntityManager;

			spawnPoints = GetComponentsInChildren<AsteroidSpawnPoint>();
		}

		private void Start()
		{
			ResetSpawnedAsteroidsNumber();
		}

		public void SpawnAsteroid_EverySomeTime(Entity asteroidPrefab)
		{
			if (Time.time - timeLastAsteroidSpawned < timeToSpawnNext)
				return;

			if (spawnedAsteroidsNumber >= howManyAsteroidsToSpawn)
				return;//spawned enough asteroids

			Entity newAsteroid = myEntityManager.Instantiate(asteroidPrefab);
			var spawnPoint = spawnPoints[UnityEngine.Random.Range(0, spawnPoints.Length)];

			//set position
			Translation trans = new Translation()
			{
				Value = spawnPoint.transform.position,
			};
			myEntityManager.AddComponentData(newAsteroid, trans);

			//set initial speed
			var randomDirection = spawnPoint.GetRandomDirection();
			Vector3 speed = randomDirection * Random.Range(initialSpeedMinMax.x, initialSpeedMinMax.y);
			PhysicsVelocity velocity = new PhysicsVelocity()
			{
				Linear = speed,
				Angular = Unity.Mathematics.float3.zero,
			};
			myEntityManager.AddComponentData(newAsteroid, velocity);
			myEntityManager.SetName(newAsteroid, "Asteroid#" + spawnedAsteroidsNumber);

			timeLastAsteroidSpawned = Time.time;
			spawnedAsteroidsNumber++;
		}

		public void ResetSpawnedAsteroidsNumber()
		{
			spawnedAsteroidsNumber = 0;
		}
	}
}