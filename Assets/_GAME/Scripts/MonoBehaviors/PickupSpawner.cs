using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using Unity.Physics;
using Unity.Transforms;
using UnityEditor;
using UnityEngine;
namespace Asteroids
{
	public class PickupSpawner : MonoBehaviour
	{
		public static PickupSpawner Instance;

		[SerializeField] private float timeToSpawnPickup = 10.0f;

		private List<Transform> spawnPoints;
		private EntityManager myEntityManager;
		private float timeLastPickupSpawned;
		private int currentSpawnpiontIndex;

		private void Awake()
		{
			Instance = this;

			myEntityManager = World.DefaultGameObjectInjectionWorld//give me world where GOs converted into entities go
				.EntityManager;

			spawnPoints = new List<Transform>();
			foreach (Transform child in this.transform)
				spawnPoints.Add(child);

			currentSpawnpiontIndex = UnityEngine.Random.Range(0, spawnPoints.Count);
		}

		public void SpawnPickup_EverySomeTime(PrefabsData prefabs)
		{
			if (Time.time - timeLastPickupSpawned < timeToSpawnPickup)
				return;

			Entity newPickup = Entity.Null;
			switch (Random.Range(0, 2))
			{
				case 0:
					newPickup = myEntityManager.Instantiate(prefabs.PickupPrefab_Shield);
					break;
				case 1:
					newPickup = myEntityManager.Instantiate(prefabs.PickupPrefab_SuperBullet);
					break;
			}

			var spawnPoint = spawnPoints[currentSpawnpiontIndex];
			currentSpawnpiontIndex++;
			if (currentSpawnpiontIndex >= spawnPoints.Count)
				currentSpawnpiontIndex = 0;

			//set position
			Translation trans = new Translation()
			{
				Value = spawnPoint.transform.position,
			};
			myEntityManager.AddComponentData(newPickup, trans);
			myEntityManager.SetName(newPickup, "Pickup");

			timeLastPickupSpawned = Time.time;
		}

#if UNITY_EDITOR
		private void OnDrawGizmos()
		{
			foreach (Transform spawnPoint in this.transform)//directions are children
			{
				Handles.color = Color.green;
				Handles.DrawWireDisc(spawnPoint.position, Vector3.forward, .1f);
			}
		}
#endif
	}
}