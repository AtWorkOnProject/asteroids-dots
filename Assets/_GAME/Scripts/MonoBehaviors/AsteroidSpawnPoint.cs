using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using UnityEditor;
using UnityEngine;
namespace Asteroids
{
	public class AsteroidSpawnPoint : MonoBehaviour
	{
		[SerializeField] private List<Transform> directions;

		private void Awake()
		{
			directions.Clear();
			foreach (Transform direction in this.transform)//directions are children
				directions.Add(direction);
		}
		public Vector3 GetRandomDirection()
		{
			var randomDirection = directions[UnityEngine.Random.Range(0, directions.Count)].position;
			return (randomDirection - this.transform.position).normalized;
		}

#if UNITY_EDITOR
		private void OnDrawGizmos()
		{
			foreach (Transform direction in this.transform)//directions are children
			{
				Handles.color = Color.yellow;
				Handles.DrawLine(this.transform.position, direction.transform.position);
				Handles.DrawWireDisc(this.transform.position, Vector3.forward, .2f);
			}
		}
#endif
	}
}