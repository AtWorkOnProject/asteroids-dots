using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using Unity.Physics;
using Unity.Transforms;
using UnityEditor;
using UnityEngine;
namespace Asteroids
{
	public class ChaserSpawner : MonoBehaviour
	{
		public static ChaserSpawner Instance;

		[SerializeField] private float timeToSpawnChaser = 20.0f;

		private List<Transform> spawnPoints;
		private EntityManager myEntityManager;
		private float timeLastPickupSpawned;
		private int currentSpawnpiontIndex;

		private void Awake()
		{
			Instance = this;

			myEntityManager = World.DefaultGameObjectInjectionWorld//give me world where GOs converted into entities go
				.EntityManager;

			spawnPoints = new List<Transform>();
			foreach (Transform child in this.transform)
				spawnPoints.Add(child);

			currentSpawnpiontIndex = UnityEngine.Random.Range(0, spawnPoints.Count);
		}

		public void SpawnChaser_EverySomeTime(PrefabsData prefabs)
		{
			if (Time.time - timeLastPickupSpawned < timeToSpawnChaser)
				return;

			Entity newChaser = myEntityManager.Instantiate(prefabs.ChaserPrefab);

			var spawnPoint = spawnPoints[currentSpawnpiontIndex];
			currentSpawnpiontIndex++;
			if (currentSpawnpiontIndex >= spawnPoints.Count)
				currentSpawnpiontIndex = 0;

			//set position
			Translation trans = new Translation()
			{
				Value = spawnPoint.transform.position,
			};
			myEntityManager.AddComponentData(newChaser, trans);
			myEntityManager.SetName(newChaser, "Chaser");

			timeLastPickupSpawned = Time.time;
		}

#if UNITY_EDITOR
		private void OnDrawGizmos()
		{
			foreach (Transform spawnPoint in this.transform)//directions are children
			{
				Handles.color = Color.red;
				Handles.DrawWireDisc(spawnPoint.position, Vector3.forward, .1f);
			}
		}
#endif
	}
}